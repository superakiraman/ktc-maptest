package com.example.dinner;

import android.app.Activity;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ViewFlipper;

public class ImageViewer extends Activity implements OnGestureListener {

	ViewFlipper flipper = null;

	GestureDetector gestureDetector = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_imageviewer);

		flipper = (ViewFlipper)findViewById(R.id.flipper);

		ImageView[] imgView = new ImageView[2];
		for (int i = 0; i < imgView.length; i++) {
			imgView[i] = new ImageView(this);

			LayoutParams params = new LayoutParams(
					LayoutParams.MATCH_PARENT,
					LayoutParams.MATCH_PARENT);
			imgView[i].setLayoutParams(params);

			flipper.addView(imgView[i]);
		}
		imgView[0].setImageResource(R.drawable.ganmo);
		imgView[1].setImageResource(R.drawable.hukiage);

		gestureDetector = new GestureDetector(this, this);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return gestureDetector.onTouchEvent(event);
	}

	@Override
	public boolean onDown(MotionEvent e) {
		// TODO 自動生成されたメソッド・スタブ
		return false;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		// 絶対値の取得
		float dx = Math.abs(velocityX);
		float dy = Math.abs(velocityY);
		// 指の移動方向(縦横)および距離の判定
		if (dx > dy && dx > 300) {
			// 指の移動方向(左右)の判定
			if (e1.getX() < e2.getX()) {
				flipper.showPrevious();
			} else {
				flipper.showNext();
			}
			return true;
		}
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {
		// TODO 自動生成されたメソッド・スタブ

	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		// TODO 自動生成されたメソッド・スタブ
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
		// TODO 自動生成されたメソッド・スタブ

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		// TODO 自動生成されたメソッド・スタブ
		return false;
	}

}
