package com.example.dinner;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.example.dinner.imageviewer.ImageViewerActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.maps.GeoPoint;

public class MainActivity extends FragmentActivity {

	Location latestLocation = null;
	LocationManager locMan = null;

	GoogleMap gMap = null;
	LocationListener locListener = null;

	GeoPoint[] locations = {
			new GeoPoint(37094038, 136726398),
			new GeoPoint(37105121, 136728426)
	};

	String[] locationNames = {
			"がんもん",
			"吹き上げの滝"
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// LocationManagerの準備
		locMan = (LocationManager)this
				.getSystemService(Context.LOCATION_SERVICE);
		// GPSから現在地の情報を取得
		latestLocation = locMan.getLastKnownLocation("gps");

		int checkGooglePlayServices = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(getApplicationContext());
		if (checkGooglePlayServices != ConnectionResult.SUCCESS) {
			// google play services is missing!!!!
			/*
			 * Returns status code indicating whether there was an error. Can be
			 * one of following in ConnectionResult: SUCCESS, SERVICE_MISSING,
			 * SERVICE_VERSION_UPDATE_REQUIRED, SERVICE_DISABLED,
			 * SERVICE_INVALID. T
			 */
			GooglePlayServicesUtil
					.getErrorDialog(checkGooglePlayServices, this, 1122)
					.show();
		}

		setUpMapIfNeeded();

		gMap.setInfoWindowAdapter(new MyInfoAdapter());
		gMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

			@Override
			public void onInfoWindowClick(Marker arg0) {
				Intent intent = new Intent(getApplicationContext(),
						ImageViewerActivity.class);
				startActivity(intent);
			}
		});

		setUpMakers();

		// animate map camera
		CameraPosition camerapos = new CameraPosition.Builder()
				.target(new LatLng(37.099749, 136.730218))
				.zoom(12.0f)
				.build();

		gMap.animateCamera(CameraUpdateFactory.newCameraPosition(camerapos));
		gMap.setMyLocationEnabled(false);
	}

	private void setUpMapIfNeeded() {
		// Do a null check to confirm that we have not already instantiated the
		// map.
		if (gMap == null) {
			// Try to obtain the map from the SupportMapFragment.
			gMap = ((SupportMapFragment)getSupportFragmentManager()
					.findFragmentById(R.id.map))
					.getMap();
		}
	}

	private void setUpMakers() {

		for (int i = 0; i < locations.length; i++) {
			MarkerOptions marker = new MarkerOptions();
			marker.position(
					new LatLng(locations[i].getLatitudeE6() / 1000000.0,
							locations[i].getLongitudeE6() / 1000000.0))
					.title(locationNames[i]);

			gMap.addMarker(marker);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private class MyInfoAdapter implements InfoWindowAdapter {

		/** Window の View. */
		private final View mView;

		public MyInfoAdapter() {
			mView = getLayoutInflater().inflate(R.layout.map_baloon, null);
		}

		@Override
		public View getInfoContents(Marker marker) {
			render(marker, mView);

			return mView;
		}

		@Override
		public View getInfoWindow(Marker marker) {
			// TODO 自動生成されたメソッド・スタブ
			return null;
		}

		/**
		 * InfoWindow を表示する.
		 * 
		 * @param marker
		 *            {@link Marker}
		 * @param view
		 *            {@link View}
		 */
		private void render(Marker marker, View view) {
			// ImageView badge = (ImageView)view.findViewById(R.id.imageView);
			// badge.setImageResource(R.drawable.ganmo);

			TextView title = (TextView)view.findViewById(R.id.textView);
			title.setText(marker.getTitle());
		}
	}

}
