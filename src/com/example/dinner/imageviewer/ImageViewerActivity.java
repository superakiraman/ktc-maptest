package com.example.dinner.imageviewer;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.example.dinner.R;
import com.example.dinner.imageviewer.fragment.MyFragmentPagerAdapter;

public class ImageViewerActivity extends FragmentActivity {

	private MyFragmentPagerAdapter pager;
	private ViewPager viewPager;

	ImageView leftArrow = null;
	ImageView rightArrow = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_imageviewer_main);

		viewPager = (ViewPager)findViewById(R.id.pager);
		pager = new MyFragmentPagerAdapter(getSupportFragmentManager(),
				getContentResolver());
		viewPager.setAdapter(pager);

		leftArrow = (ImageView)findViewById(R.id.imageView1);
		rightArrow = (ImageView)findViewById(R.id.imageView2);

		leftArrow.setVisibility(View.INVISIBLE);

		leftArrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				viewPager.setCurrentItem(0);
			}
		});

		rightArrow.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				viewPager.setCurrentItem(1);

			}
		});

		viewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				if (arg0 == 0) {
					leftArrow.setVisibility(View.INVISIBLE);
				} else {
					leftArrow.setVisibility(View.VISIBLE);
				}

				if (arg0 == 1) {
					rightArrow.setVisibility(View.INVISIBLE);
				} else {
					rightArrow.setVisibility(View.VISIBLE);
				}
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO 自動生成されたメソッド・スタブ

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO 自動生成されたメソッド・スタブ

			}
		});
	}
}
