package com.example.dinner.imageviewer.fragment;

import android.content.ContentResolver;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.dinner.R;

public class MyFragmentPagerAdapter extends FragmentPagerAdapter {
	private int[] resList;

	public MyFragmentPagerAdapter(FragmentManager fm, ContentResolver resolver) {
		super(fm);
		// getFileList(resolver);

		resList = new int[] {
				R.drawable.ganmo,
				R.drawable.hukiage
		};
	}

	@Override
	public Fragment getItem(int position) {
		if (resList.length == 0 || resList.length < position) { return null; }

		ImageFragment imf = new ImageFragment();
		Bundle bundle = new Bundle();
		bundle.putInt("rId", resList[position]);

		imf.setArguments(bundle);

		return imf;
	}

	@Override
	public int getCount() {
		return resList.length;
	}
}
