package com.example.dinner.imageviewer.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;

import com.example.dinner.R;

public class ImageFragment extends Fragment {

	private int resourceId = 0;
	private String fileName;

	public ImageFragment() {

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		fileName = getArguments().getString("fileName");
		resourceId = getArguments().getInt("rId", 0);

		LinearLayout layout = (LinearLayout)inflater
				.inflate(R.layout.fragment_image, null);

		ImageView image = (ImageView)layout.findViewById(R.id.image);
		image.setImageResource(resourceId);
		image.setScaleType(ScaleType.FIT_CENTER);
		return layout;
	}
}
