package com.example.dinner.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dinner.R;

public class DetailFragment extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map_baloon);

		ImageView badge = (ImageView)findViewById(R.id.imageView);
		badge.setImageResource(R.drawable.ganmo);

		TextView title = (TextView)findViewById(R.id.textView);
		title.setText("でぃす　いず　がんもん");
	}
}
